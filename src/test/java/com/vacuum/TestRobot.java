package com.vacuum;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Before;
import org.junit.Test;

public class TestRobot {

    RunRobot runRobot;
    Input input1;
    Input input2;

    String userInput1 = "M:-10,10,-10,10;S:0,0;[W5,E5,N4,E3,S2,W1]";
    String userInput2 = "M:-8,15,-10,10;S:4,2;[W5,E5,N4,E4,S7,W6]";

    @Before
    public void setUp() throws Exception {
        runRobot = new RunRobot();
        input1 = new Input(userInput1);
        input2 = new Input(userInput2);
    }

    @Test
    public void testRunRobot() {
        String[] directions = input1.directionInput();
        int[] maxPos = input1.formatStartPosAndMaxPos(0);
        int[] startingPos = input1.formatStartPosAndMaxPos(1);
        runRobot.runRobot(directions, maxPos, startingPos);

        String[] direction2 = input2.directionInput();
        int[] maxAndMin2 = input2.formatStartPosAndMaxPos(0);
        int[] startingPosition2 = input2.formatStartPosAndMaxPos(1);
        runRobot.runRobot(direction2, maxAndMin2, startingPosition2);
    }

    @Test
    public void testInputFormating() {

        int[] maxAndMin = { -10, 10, -10, 10 };
        int[] startingPos = { 0, 0 };
        String[] dir = { "W5", "E5", "N4", "E3", "S2", "W1" };

        assertArrayEquals(maxAndMin, input1.formatStartPosAndMaxPos(0));
        assertArrayEquals(startingPos, input1.formatStartPosAndMaxPos(1));
        assertArrayEquals(dir, input1.directionInput());

    }
}
