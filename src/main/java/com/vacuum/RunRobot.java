package com.vacuum;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;
import java.io.*;

class RunRobot {

    public static void main(String[] args) {

        RunRobot runBot = new RunRobot();

        // M:-10,10,-10,10;S:0,0;[W5,E5,N4,E3,S2,W1] Måste skickas in i detta format
        Input userInput = new Input(runBot.getInput());
        String[] directionArr = userInput.directionInput();
        int[] maxAndMin = userInput.formatStartPosAndMaxPos(0);
        int[] startingPosition = userInput.formatStartPosAndMaxPos(1);

        runBot.runRobot(directionArr, maxAndMin, startingPosition);
    }

    public String getInput() {

        Console console = System.console();
        String input = console.readLine(
                "Enter data in following format : M:MinX,MaxX,MinY,MaxY;S:StartingX,StartingY;[Direction+Length] : ");

        return input;
    }

    public void runRobot(String[] robotRoute, int[] maxPos, int[] startingPos) {

        int posX = startingPos[0];
        int posY = startingPos[1];

        ArrayList<String> fullPath = new ArrayList<String>();

        for (int i = 0; i < robotRoute.length; i++) {

            for (int j = 0; j < Integer.parseInt(robotRoute[i].substring(1)); j++) {

                if (posX <= maxPos[0] || posX >= maxPos[1] || posY <= maxPos[2] || posY >= maxPos[3]) {

                    System.out.println("Robot went out of bounds");
                    printPath(fullPath);
                    return;
                }

                if (robotRoute[i].startsWith("S")) {
                    posY -= 1;
                } else if (robotRoute[i].startsWith("N")) {
                    posY += 1;
                } else if (robotRoute[i].startsWith("E")) {
                    posX += 1;
                } else {
                    posX -= 1;
                }
                fullPath.add(Integer.toString(posX) + "," + Integer.toString(posY) + ";");
            }
        }
        printPath(fullPath);

    }

    public void printPath(ArrayList<String> pathList) {
        // -5,5;-6,5;-7,5;-8,5;-9,5;-10,5;
        System.out.println("All Positions Cleaned:");
        System.out.println(formatPath(pathList));
        Set<String> set = new LinkedHashSet<>();
        set.addAll(pathList);
        pathList.clear();
        pathList.addAll(set);
        System.out.println("Unique Positions Cleaned:");
        System.out.println(formatPath(pathList));
    }

    public String formatPath(ArrayList<String> pathList) {

        StringBuilder builder = new StringBuilder();
        for (String value : pathList) {
            builder.append(value);
        }
        String text = builder.toString();
        return text;
    }
}
