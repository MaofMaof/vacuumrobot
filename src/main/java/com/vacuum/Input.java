package com.vacuum;

class Input {

    String input;

    public Input(String input) {
        this.input = input;
    }

    public String[] directionInput() {

        String[] inputToArr = input.split(";");
        String gg = inputToArr[2];
        String[] arr = gg.replace("[", "").replace("]", "").split(",");
        return arr;
    }

    public int[] formatStartPosAndMaxPos(int index) { // 0, 1
        String[] inputToArr = input.split(";");
        String gg = inputToArr[index];
        String[] arr = gg.substring(2).split(",");

        int[] intArr = new int[arr.length];

        for (int i = 0; i < arr.length; i++) {
            intArr[i] = Integer.parseInt(arr[i]);
        }

        return intArr;
    }

    

}